<?php

namespace App\Controller\MemberSpace\Registration;

use App\Entity\User;
use App\Form\FormInscriptionType;
use App\Repository\ArticlesRepository;
use App\Repository\UserRepository;
use App\Security\LoginFormAuthenticator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;
use SymfonyCasts\Bundle\VerifyEmail\Exception\VerifyEmailExceptionInterface;
use SymfonyCasts\Bundle\VerifyEmail\VerifyEmailHelperInterface;

class RegistrationController extends AbstractController
{
    protected $repository, $em;
  
    public function __construct(ArticlesRepository $repository, EntityManagerInterface $em)
    {
       

        $this->repository = $repository;
        $this->em = $em;
    }

    public function index(Request $request, UserPasswordEncoderInterface $passwordEncoder,\Swift_Mailer $mailer)
    {   
       
        $user = new User();
        $form = $this->createForm(FormInscriptionType::class, $user);


        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('password')->getData()
                )
            );
    
            $user->setRoles(['roles' => 'ROLE_USER']);

            $a = $form->getData();
        
            $user->setToken(md5(uniqid()));

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

                    $message = (new \Swift_Message('Nouveau compte'))

                    ->setFrom('x@sfr.fr')
                    ->setTo($user->getEmail())
                    ->setBody(
                        $this->renderView(
                            'SpaceMember/signin/confirmation_email.html.twig', ['token' => $user->getToken()]
                        ),
                        'text/html'
                    )
                ;
                $mailer->send($message);
                $this->addFlash('success', 'Email envoyé');
        }
        
        return $this->render('SpaceMember/signin/index.html.twig', [
            'form' => $form->createView()
        ]);
        
    }
    public function verifEmail($token, UserRepository $user)
    {

        $user = $user->findOneBy(['token' => $token]);
        if(!$user)
        {
            throw $this->createNotFoundException('Cet utilisateur n\'existe pas');
        }        

            // On supprime le token
        $user->setToken('null');
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($user);
        $entityManager->flush();

        $this->addFlash('success', 'Utilisateur activé avec succès');

        return $this->redirectToRoute('member_space');

    }



  

  

}