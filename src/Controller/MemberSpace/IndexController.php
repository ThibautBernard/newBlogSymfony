<?php


namespace App\Controller\MemberSpace;

use App\Entity\Articles;
use App\Form\FormArticleType;
use App\Repository\ArticlesRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
class IndexController extends AbstractController
{

   protected $repositoryArticles, $em, $repoUser;
    
    public function __construct(ArticlesRepository $repository, UserRepository $repoUser, EntityManagerInterface $em)
    {
        $this->repositoryArticles = $repository;
        $this->repoUser = $repoUser;

        $this->em = $em;
    }


    public function index(PaginatorInterface $paginator, Request $request)
    {   
        $articles = $this->repositoryArticles->findAll();

        $pagination = $paginator->paginate(
            $articles,
            $request->query->getInt('page', 1), 4
        );

        $lastArticlesCreated = $this->repositoryArticles->lastArticlesDone();
        $lastUserRegister = $this->repoUser->lastUsersRegister();
        $user = $this->getUser();
      
        return $this->render('SpaceMember/Home/index.html.twig',[
            'lastArticlesCreated' => $lastArticlesCreated,
            'lastUsersRegister' =>  $lastUserRegister,
            'user' => $user,
            'pagination' => $pagination
        
        ]);  

          
        
        
            

    }

  

}