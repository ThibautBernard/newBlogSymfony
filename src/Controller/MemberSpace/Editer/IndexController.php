<?php


namespace App\Controller\MemberSpace\Editer;

use App\Entity\Articles;
use App\Entity\User;
use App\Form\FormArticleType;
use App\Repository\ArticlesRepository;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class IndexController extends AbstractController
{


  

    public function index(ArticlesRepository $repoArticle, Request $request)
    {
        $user = $this->getUser();
        $article = new Articles;
        $article->setAuthorName($user->getUsername());
        $article->setUpdatedDate(new DateTime());
        $article->setCreationDate(new DateTime());

        $form = $this->createForm(FormArticleType::class, $article);

        //$error = $authenticationUtils->getLastAuthenticationError();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid() && !($form->isEmpty()))
        {
            if($this->getUser())
            {
                if($repoArticle->selectArticleByName($article->getArticleTitle() < 0))
                {
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($article);
                    $em->flush();
                    $this->addFlash('success', 'Article Created!');
                }
                else{
                    $this->addFlash('warning', 'Nom d\'article déjà existant');

                }
                
            }
        }
        return $this->render('SpaceMember/Add_News/pageAddNews.html.twig', [
            'form' => $form->createView(),
            'user' => $user
        ]);
    }



}