<?php

namespace App\Controller\MemberSpace\Editer\Admin;

use App\Entity\Articles;
use App\Form\FormArticleType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class EditerController extends AbstractController
{


    public function updateArticle(Articles $articles, Request $request)
    {

        $repositoryArticle = $this->getDoctrine()->getRepository(Articles::class);

        $form = $this->createForm(FormArticleType::class, $articles);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            //$em->persist($user);
            $em->flush();
            $this->addFlash('updateArticle', 'L\'article à été modifié');

            return $this->redirectToRoute('member_space');

        }
        return $this->render('SpaceMember/Add_News/pageAddNews.html.twig', ['form' => $form->createView()]);
    }

    public function deleteOneArticle(Articles $articles, Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($articles);
        $entityManager->flush();
        $this->addFlash('deleteArticle', 'L\'article à été supprimé');

        return $this->redirectToRoute('member_space');
        
    }



}