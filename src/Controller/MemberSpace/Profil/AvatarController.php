<?php

namespace App\Controller\MemberSpace\Profil;

use App\Entity\Profile;
use App\Entity\User;
use App\Form\AvatarFormType;
use App\Form\UpdateRolesType;
use App\Repository\ProfileRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Profiler\Profile as ProfilerProfile;
use Symfony\Component\Security\Core\User\UserInterface;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Component\Serializer\Serializer;

class AvatarController extends AbstractController
{

    protected $profileRepo;

    public function __construct(ProfileRepository $profileRepository)
    {
        $this->profileRepo = $profileRepository;
    }

    public function addAvatar($username, Request $request)
    {
        $user = $this->getUser();
        $profile = new Profile();

        if ($username != $user->getUsername()) {
            $this->addFlash('not_access', 'Vous n\'avez pas le droit d\'allez ici ');

            return $this->redirectToRoute('app_loginMember');
        }

            $profile->setUser($user); //give username of the current user, actually the user object 
            $profile->setUpdatedImgAt( new DateTime('NOW') );

            $form = $this->createForm(AvatarFormType::class, $profile);
    
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid())
             {
                        $em = $this->getDoctrine()->getManager();
                        $em->persist($profile);
                        $em->flush();
                        $this->addFlash('add', 'Votre avatar à été ajouté');
                        return $this->redirectToRoute('profil', array('username' => $user->getUsername()));
            }
       
        return $this->render('SpaceMember/Profil/Avatar/uploadAvatar.html.twig', [
            'form' => $form->createView(),
            
        ]);
    }

    public function deleteAvatar($id, Request $request)
    {
        $user = $this->getUser();


        $repository = $this->getDoctrine()->getRepository(Profile::class);
        $profile = $repository->findOneBy(['user' => $id]);
        

        if($id == $user->getId())
        {
            $entityManager = $this->getDoctrine()->getManager();

            $entityManager->remove($profile);
            $entityManager->flush();
            $this->addFlash('deletedAvatard', 'Votre avatar à été supprimé');

            return $this->redirectToRoute('profil', array('username' => $user->getUsername()));

        }
       return $this->redirectToRoute('home');

    }

   


}