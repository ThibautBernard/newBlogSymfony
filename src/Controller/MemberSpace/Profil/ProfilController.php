<?php 


namespace App\Controller\MemberSpace\Profil;

use App\Entity\Profile;
use App\Entity\User;
use App\Form\UpdateFormType;
use App\Form\UpdateRolesType;
use App\Repository\ProfileRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
class ProfilController extends AbstractController
{

    protected $repoUser, $em, $session;

    public function __construct(UserRepository $repo, EntityManagerInterface $em, SessionInterface $session)
    {
        $this->repoUser = $repo;
        $this->em = $em;
        $this->session = $session;
    }

    public function index($username, ProfileRepository $profileRepository)
    {

        $owner = $this->getUser();

       
        $profileB = $profileRepository->findOneBy(['user' => $owner->getId()]);
        $user = $this->repoUser->userByHisUsername($owner->getUsername());

        if(empty($profileB))
        {
            return $this->render('SpaceMember/Profil/home.html.twig', [
                'user' => $user,
            ]);
        }

        return $this->render('SpaceMember/Profil/home.html.twig', [
            'user' => $user,
            'profile' => $profileB
        ]);
    }

   

    public function visitorProfil($username, Request $request, ProfileRepository $profileRepository)
    {

        $user = $this->repoUser->userByHisUsername($username);

        //objet user pour récupérer l'id et avoir l'avatar
        $idUser = $this->repoUser->findOneBy(['username' => $username]);
        $profileB = $profileRepository->findOneBy(['user' => $idUser->getId()]);

        //si n'a pas d'avatar en bdd 
        if(empty($profileB))
        {
            return $this->render('SpaceMember/Profil/homeVisiteur.html.twig', [
                'user' => $user,
            ]);
        }
       
            return $this->render('SpaceMember/Profil/homeVisiteur.html.twig', [
                'user' => $user,
                'profile' => $profileB

            ]);
        


    }

     public function deleteProfil($username)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $user = $entityManager->getRepository(User::class)->findOneBy(['username' => $username]);
        $owner = $this->getUser();


        if (!$user) {
            throw $this->createNotFoundException(
                'Ce compte n\'existe pas '.$username
            );
        }
       
        if($owner->getUsername() == $username OR $this->isGranted('ROLE_ADMIN'))
        {
            $entityManager->remove($user);
            $entityManager->flush();
            $session = new Session();
            $session->invalidate();
            $this->addFlash('sup', 'Votre compte a bien été supprimée');

            return $this->redirectToRoute('app_loginMember');
        }
        else{
            throw $this->createNotFoundException(
                'Vous ne pouvez pas supprimé ce compte '
            );
            $this->addFlash('error', 'Vous ne pouvez pas supprimé un compte sur');

        }
       
    }

    public function updateProfil(User $user, Request $request)
    {   

        $form = $this->createForm(UpdateFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
                    $this->getDoctrine()->getManager()->flush();
                    $this->addFlash('modif', 'Vos informations ont bien été modifiés');
            
                    return $this->redirectToRoute('profil', array('username' => $user->getUsername()));               
        }
    
        return $this->render('SpaceMember/Profil/Update/index.html.twig', [
            'form' => $form->createView(),
        ]);

    }


    public function updateRoles($username, Request $request)
    {
        $user = $this->getUser();

        $repository = $this->getDoctrine()->getRepository(User::class);
        $user = $repository->findOneBy(['username' => $username]);


        $form = $this->createForm(UpdateRolesType::class, $user);


        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
         {
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($user);
                    $em->flush();
                    $this->addFlash('modifRoles', 'Le rôle à été changé');

                    return $this->redirectToRoute('visitor_profil', array('username' => $username));
        }
        return $this->render('SpaceMember/Profil/Update/updatesRoles.html.twig', ['form' => $form->createView(),]);



    }




    
}