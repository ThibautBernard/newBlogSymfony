<?php


namespace App\Controller\Home;

use App\Entity\Articles;
use App\Form\FormArticleType;
use App\Repository\ArticlesRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
//use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class IndexController extends AbstractController
{
    protected $repository, $em;
    
    public function __construct(ArticlesRepository $repository, EntityManagerInterface $em)
    {
        $this->repository = $repository;
        $this->em = $em;
    }

    public function index(Request $request)
    {   
        $articles = $this->repository->selectNumberOfArticles(6);
        return $this->render('Home/index.html.twig', [
            'article' => $articles
        ]);
        
    }

    public function articlePage($index)
    {

        $articles = $this->repository->selectArticleById($index);

        return $this->render('Home/article.html.twig', [
            'article' => $articles
        ]);

    }


}