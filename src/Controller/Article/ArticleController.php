<?php

namespace App\Controller\Article;

use App\Entity\Articles;
use App\Repository\ArticlesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ArticleController extends AbstractController
{
    protected $repo;
    
    public function __construct(ArticlesRepository $repo)
    {
        $this->repo = $repo;

    }

    public function deleteOneNews($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $article = $entityManager->getRepository(Articles::class)->find($id);
        
        if (!$article) {
            throw $this->createNotFoundException(
                'No product found for id '.$id
            );
        }
        $entityManager->remove($article);
        $entityManager->flush();
        $this->addFlash('success', 'Article supprimé');

        return $this->redirectToRoute('home');
        


    }


}