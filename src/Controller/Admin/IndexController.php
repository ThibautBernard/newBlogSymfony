<?php


namespace App\Controller\Admin;

use App\Entity\Articles;
use App\Form\FormArticleType;
use App\Repository\ArticlesRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class IndexController extends AbstractController
{
    protected $repository, $em;
    
    public function __construct(ArticlesRepository $repository, EntityManagerInterface $em)
    {
        $this->repository = $repository;
        $this->em = $em;
    }

    public function index(PaginatorInterface $paginator, Request $request)
    {
        $articles = $this->repository->findAll();

        $pagination = $paginator->paginate(
            $articles,
            $request->query->getInt('page', 1),
            4
        );

        
        $user = $this->getUser();
      
        return $this->render('Admin/index.html.twig', [
            'user' => $user,
            'pagination' => $pagination
        
        ]);
    }
    
}