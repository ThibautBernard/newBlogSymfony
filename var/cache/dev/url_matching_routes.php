<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/_profiler' => [[['_route' => '_profiler_home', '_controller' => 'web_profiler.controller.profiler::homeAction'], null, null, null, true, false, null]],
        '/_profiler/search' => [[['_route' => '_profiler_search', '_controller' => 'web_profiler.controller.profiler::searchAction'], null, null, null, false, false, null]],
        '/_profiler/search_bar' => [[['_route' => '_profiler_search_bar', '_controller' => 'web_profiler.controller.profiler::searchBarAction'], null, null, null, false, false, null]],
        '/_profiler/phpinfo' => [[['_route' => '_profiler_phpinfo', '_controller' => 'web_profiler.controller.profiler::phpinfoAction'], null, null, null, false, false, null]],
        '/_profiler/open' => [[['_route' => '_profiler_open_file', '_controller' => 'web_profiler.controller.profiler::openAction'], null, null, null, false, false, null]],
        '/' => [[['_route' => 'home', '_controller' => 'App\\Controller\\Home\\IndexController::index'], null, null, null, false, false, null]],
        '/member' => [[['_route' => 'member_space', '_controller' => 'App\\Controller\\MemberSpace\\IndexController::index'], null, null, null, false, false, null]],
        '/loginmember' => [[['_route' => 'app_loginMember', '_controller' => 'App\\Controller\\MemberSpace\\SecurityController::login'], null, null, null, false, false, null]],
        '/logoutmember' => [[['_route' => 'app_logoutMember', '_controller' => 'App\\Controller\\MemberSpace\\SecurityController::logout'], null, null, null, false, false, null]],
        '/signin' => [[['_route' => 'registration', '_controller' => 'App\\Controller\\MemberSpace\\Registration\\RegistrationController:index'], null, null, null, false, false, null]],
        '/member/add' => [[['_route' => 'add_news', '_controller' => 'App\\Controller\\MemberSpace\\Editer\\IndexController:index'], null, null, null, false, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/_(?'
                    .'|error/(\\d+)(?:\\.([^/]++))?(*:38)'
                    .'|wdt/([^/]++)(*:57)'
                    .'|profiler/([^/]++)(?'
                        .'|/(?'
                            .'|search/results(*:102)'
                            .'|router(*:116)'
                            .'|exception(?'
                                .'|(*:136)'
                                .'|\\.css(*:149)'
                            .')'
                        .')'
                        .'|(*:159)'
                    .')'
                .')'
                .'|/article/(?'
                    .'|([^/]++)(*:189)'
                    .'|delete/([^/]++)(*:212)'
                .')'
                .'|/verif/([^/]++)(*:236)'
                .'|/member/(?'
                    .'|profil/(?'
                        .'|([^/]++)(*:273)'
                        .'|delete/([^/]++)(*:296)'
                    .')'
                    .'|up(?'
                        .'|date(?'
                            .'|/([^/]++)(*:326)'
                            .'|Roles/([^/]++)(*:348)'
                        .')'
                        .'|load/([^/]++)(*:370)'
                    .')'
                    .'|deleteAvatar/([^/]++)(*:400)'
                    .'|visitor/([^/]++)(*:424)'
                    .'|article/(?'
                        .'|([^/]++)/update(*:458)'
                        .'|delete/([^/]++)(*:481)'
                    .')'
                .')'
            .')/?$}sDu',
    ],
    [ // $dynamicRoutes
        38 => [[['_route' => '_preview_error', '_controller' => 'error_controller::preview', '_format' => 'html'], ['code', '_format'], null, null, false, true, null]],
        57 => [[['_route' => '_wdt', '_controller' => 'web_profiler.controller.profiler::toolbarAction'], ['token'], null, null, false, true, null]],
        102 => [[['_route' => '_profiler_search_results', '_controller' => 'web_profiler.controller.profiler::searchResultsAction'], ['token'], null, null, false, false, null]],
        116 => [[['_route' => '_profiler_router', '_controller' => 'web_profiler.controller.router::panelAction'], ['token'], null, null, false, false, null]],
        136 => [[['_route' => '_profiler_exception', '_controller' => 'web_profiler.controller.exception_panel::body'], ['token'], null, null, false, false, null]],
        149 => [[['_route' => '_profiler_exception_css', '_controller' => 'web_profiler.controller.exception_panel::stylesheet'], ['token'], null, null, false, false, null]],
        159 => [[['_route' => '_profiler', '_controller' => 'web_profiler.controller.profiler::panelAction'], ['token'], null, null, false, true, null]],
        189 => [[['_route' => 'article', '_controller' => 'App\\Controller\\Home\\IndexController::articlePage'], ['index'], null, null, false, true, null]],
        212 => [[['_route' => 'delete_one_news', '_controller' => 'App\\Controller\\Article\\ArticleController:deleteOneNews'], ['id'], null, null, false, true, null]],
        236 => [[['_route' => 'registration_confirmation_route', '_controller' => 'App\\Controller\\MemberSpace\\Registration\\RegistrationController:verifEmail'], ['token'], null, null, false, true, null]],
        273 => [[['_route' => 'profil', '_controller' => 'App\\Controller\\MemberSpace\\Profil\\ProfilController:index'], ['username'], null, null, false, true, null]],
        296 => [[['_route' => 'profil_delete', '_controller' => 'App\\Controller\\MemberSpace\\Profil\\ProfilController:deleteProfil'], ['username'], null, null, false, true, null]],
        326 => [[['_route' => 'update', '_controller' => 'App\\Controller\\MemberSpace\\Profil\\ProfilController:updateProfil'], ['username'], null, null, false, true, null]],
        348 => [[['_route' => 'update_roles', '_controller' => 'App\\Controller\\MemberSpace\\Profil\\ProfilController:updateRoles'], ['username'], null, null, false, true, null]],
        370 => [[['_route' => 'avatar', '_controller' => 'App\\Controller\\MemberSpace\\Profil\\AvatarController:addAvatar'], ['username'], null, null, false, true, null]],
        400 => [[['_route' => 'delete_avatar', '_controller' => 'App\\Controller\\MemberSpace\\Profil\\AvatarController:deleteAvatar'], ['id'], null, null, false, true, null]],
        424 => [[['_route' => 'visitor_profil', '_controller' => 'App\\Controller\\MemberSpace\\Profil\\ProfilController:visitorProfil'], ['username'], null, null, false, true, null]],
        458 => [[['_route' => 'updateArticle', '_controller' => 'App\\Controller\\MemberSpace\\Editer\\Admin\\EditerController:updateArticle'], ['id'], null, null, false, false, null]],
        481 => [
            [['_route' => 'deleteOneArticle', '_controller' => 'App\\Controller\\MemberSpace\\Editer\\Admin\\EditerController:deleteOneArticle'], ['id'], null, null, false, true, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
