<?php

// This file has been auto-generated by the Symfony Routing Component.

return [
    '_preview_error' => [['code', '_format'], ['_controller' => 'error_controller::preview', '_format' => 'html'], ['code' => '\\d+'], [['variable', '.', '[^/]++', '_format', true], ['variable', '/', '\\d+', 'code', true], ['text', '/_error']], [], []],
    '_wdt' => [['token'], ['_controller' => 'web_profiler.controller.profiler::toolbarAction'], [], [['variable', '/', '[^/]++', 'token', true], ['text', '/_wdt']], [], []],
    '_profiler_home' => [[], ['_controller' => 'web_profiler.controller.profiler::homeAction'], [], [['text', '/_profiler/']], [], []],
    '_profiler_search' => [[], ['_controller' => 'web_profiler.controller.profiler::searchAction'], [], [['text', '/_profiler/search']], [], []],
    '_profiler_search_bar' => [[], ['_controller' => 'web_profiler.controller.profiler::searchBarAction'], [], [['text', '/_profiler/search_bar']], [], []],
    '_profiler_phpinfo' => [[], ['_controller' => 'web_profiler.controller.profiler::phpinfoAction'], [], [['text', '/_profiler/phpinfo']], [], []],
    '_profiler_search_results' => [['token'], ['_controller' => 'web_profiler.controller.profiler::searchResultsAction'], [], [['text', '/search/results'], ['variable', '/', '[^/]++', 'token', true], ['text', '/_profiler']], [], []],
    '_profiler_open_file' => [[], ['_controller' => 'web_profiler.controller.profiler::openAction'], [], [['text', '/_profiler/open']], [], []],
    '_profiler' => [['token'], ['_controller' => 'web_profiler.controller.profiler::panelAction'], [], [['variable', '/', '[^/]++', 'token', true], ['text', '/_profiler']], [], []],
    '_profiler_router' => [['token'], ['_controller' => 'web_profiler.controller.router::panelAction'], [], [['text', '/router'], ['variable', '/', '[^/]++', 'token', true], ['text', '/_profiler']], [], []],
    '_profiler_exception' => [['token'], ['_controller' => 'web_profiler.controller.exception_panel::body'], [], [['text', '/exception'], ['variable', '/', '[^/]++', 'token', true], ['text', '/_profiler']], [], []],
    '_profiler_exception_css' => [['token'], ['_controller' => 'web_profiler.controller.exception_panel::stylesheet'], [], [['text', '/exception.css'], ['variable', '/', '[^/]++', 'token', true], ['text', '/_profiler']], [], []],
    'home' => [[], ['_controller' => 'App\\Controller\\Home\\IndexController::index'], [], [['text', '/']], [], []],
    'article' => [['index'], ['_controller' => 'App\\Controller\\Home\\IndexController::articlePage'], [], [['variable', '/', '[^/]++', 'index', true], ['text', '/article']], [], []],
    'member_space' => [[], ['_controller' => 'App\\Controller\\MemberSpace\\IndexController::index'], [], [['text', '/member']], [], []],
    'app_loginMember' => [[], ['_controller' => 'App\\Controller\\MemberSpace\\SecurityController::login'], [], [['text', '/loginmember']], [], []],
    'app_logoutMember' => [[], ['_controller' => 'App\\Controller\\MemberSpace\\SecurityController::logout'], [], [['text', '/logoutmember']], [], []],
    'registration' => [[], ['_controller' => 'App\\Controller\\MemberSpace\\Registration\\RegistrationController:index'], [], [['text', '/signin']], [], []],
    'registration_confirmation_route' => [['token'], ['_controller' => 'App\\Controller\\MemberSpace\\Registration\\RegistrationController:verifEmail'], [], [['variable', '/', '[^/]++', 'token', true], ['text', '/verif']], [], []],
    'add_news' => [[], ['_controller' => 'App\\Controller\\MemberSpace\\Editer\\IndexController:index'], [], [['text', '/member/add']], [], []],
    'delete_one_news' => [['id'], ['_controller' => 'App\\Controller\\Article\\ArticleController:deleteOneNews'], [], [['variable', '/', '[^/]++', 'id', true], ['text', '/article/delete']], [], []],
    'profil' => [['username'], ['_controller' => 'App\\Controller\\MemberSpace\\Profil\\ProfilController:index'], [], [['variable', '/', '[^/]++', 'username', true], ['text', '/member/profil']], [], []],
    'profil_delete' => [['username'], ['_controller' => 'App\\Controller\\MemberSpace\\Profil\\ProfilController:deleteProfil'], [], [['variable', '/', '[^/]++', 'username', true], ['text', '/member/profil/delete']], [], []],
    'update' => [['username'], ['_controller' => 'App\\Controller\\MemberSpace\\Profil\\ProfilController:updateProfil'], [], [['variable', '/', '[^/]++', 'username', true], ['text', '/member/update']], [], []],
    'avatar' => [['username'], ['_controller' => 'App\\Controller\\MemberSpace\\Profil\\AvatarController:addAvatar'], [], [['variable', '/', '[^/]++', 'username', true], ['text', '/member/upload']], [], []],
    'delete_avatar' => [['id'], ['_controller' => 'App\\Controller\\MemberSpace\\Profil\\AvatarController:deleteAvatar'], [], [['variable', '/', '[^/]++', 'id', true], ['text', '/member/deleteAvatar']], [], []],
    'update_roles' => [['username'], ['_controller' => 'App\\Controller\\MemberSpace\\Profil\\ProfilController:updateRoles'], [], [['variable', '/', '[^/]++', 'username', true], ['text', '/member/updateRoles']], [], []],
    'visitor_profil' => [['username'], ['_controller' => 'App\\Controller\\MemberSpace\\Profil\\ProfilController:visitorProfil'], [], [['variable', '/', '[^/]++', 'username', true], ['text', '/member/visitor']], [], []],
    'updateArticle' => [['id'], ['_controller' => 'App\\Controller\\MemberSpace\\Editer\\Admin\\EditerController:updateArticle'], [], [['text', '/update'], ['variable', '/', '[^/]++', 'id', true], ['text', '/member/article']], [], []],
    'deleteOneArticle' => [['id'], ['_controller' => 'App\\Controller\\MemberSpace\\Editer\\Admin\\EditerController:deleteOneArticle'], [], [['variable', '/', '[^/]++', 'id', true], ['text', '/member/article/delete']], [], []],
];
