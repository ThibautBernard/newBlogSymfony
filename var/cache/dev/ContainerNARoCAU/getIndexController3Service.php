<?php

namespace ContainerNARoCAU;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getIndexController3Service extends App_KernelDevDebugContainer
{
    /**
     * Gets the public 'App\Controller\MemberSpace\Editer\IndexController' shared autowired service.
     *
     * @return \App\Controller\MemberSpace\Editer\IndexController
     */
    public static function do($container, $lazyLoad = true)
    {
        include_once \dirname(__DIR__, 4).'/vendor/symfony/framework-bundle/Controller/AbstractController.php';
        include_once \dirname(__DIR__, 4).'/src/Controller/MemberSpace/Editer/IndexController.php';

        $container->services['App\\Controller\\MemberSpace\\Editer\\IndexController'] = $instance = new \App\Controller\MemberSpace\Editer\IndexController();

        $instance->setContainer(($container->privates['.service_locator.g9CqTPp'] ?? $container->load('get_ServiceLocator_G9CqTPpService'))->withContext('App\\Controller\\MemberSpace\\Editer\\IndexController', $container));

        return $instance;
    }
}
