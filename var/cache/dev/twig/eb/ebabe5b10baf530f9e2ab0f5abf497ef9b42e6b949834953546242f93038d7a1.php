<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* Home/index.html.twig */
class __TwigTemplate_a2187275da9ad0a9792899e93e96f93d875c117f0b54cdf59f0dfdb88c0a0876 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "Home/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "Home/index.html.twig"));

        // line 1
        $this->loadTemplate("_nav.html.twig", "Home/index.html.twig", 1)->display($context);
        echo " 

";
        // line 3
        $this->displayBlock('body', $context, $blocks);
        // line 35
        echo "
<style>
.container_search
{
    margin-top: 2%;
    width: 100%;
    position: flex;
}
.container_under
{
    width: 30%;
    margin: auto;
    
}

.container_article
{
    height: 100%;
}
.container_card
{
    display: flex;
    margin: 5%;
    flex-wrap: wrap;
}
.card
{
    margin-top: 2%;
    margin-left: 5%;
}

.card-text
{
    white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  max-width: 200px;
}
</style>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
 ";
        // line 5
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 5, $this->source); })()), "flashes", [0 => "success"], "method", false, false, false, 5));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 6
            echo "            <div class=\"alert alert-success\">
                ";
            // line 7
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "
            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 10
        echo "

    <div class=\"container_article\">

        
        <div class=\"container_card\">
        ";
        // line 16
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["article"]) || array_key_exists("article", $context) ? $context["article"] : (function () { throw new RuntimeError('Variable "article" does not exist.', 16, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["articles"]) {
            // line 17
            echo "            <div class=\"card\" style=\"width: 18rem;\">
                <img src=\"";
            // line 18
            echo twig_escape_filter($this->env, $this->extensions['Vich\UploaderBundle\Twig\Extension\UploaderExtension']->asset($context["articles"]), "html", null, true);
            echo "\" width=\"18rem\" height=\"20%\"class=\"card-img-top\" alt=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["articles"], "imageName", [], "any", false, false, false, 18), "html", null, true);
            echo "\">
                <div class=\"card-body\">
                    <h5 class=\"card-title\">";
            // line 20
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["articles"], "articleTitle", [], "any", false, false, false, 20), "html", null, true);
            echo "</h5>
                    <p class=\"card-text\">";
            // line 21
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["articles"], "contentArticle", [], "any", false, false, false, 21), "html", null, true);
            echo "</p>
                    <a href=\"";
            // line 22
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("article", ["index" => twig_get_attribute($this->env, $this->source, $context["articles"], "id", [], "any", false, false, false, 22)]), "html", null, true);
            echo "\" class=\"btn btn-primary\">Voir l'article</a>
                </div>
            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['articles'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 26
        echo "
        </div>
        

    </div>

    

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "Home/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  168 => 26,  158 => 22,  154 => 21,  150 => 20,  143 => 18,  140 => 17,  136 => 16,  128 => 10,  119 => 7,  116 => 6,  112 => 5,  109 => 4,  99 => 3,  51 => 35,  49 => 3,  44 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% include(\"_nav.html.twig\") %} 

{% block body  %}

 {% for message in app.flashes('success') %}
            <div class=\"alert alert-success\">
                {{ message }}
            </div>
        {% endfor %}


    <div class=\"container_article\">

        
        <div class=\"container_card\">
        {% for articles in article %}
            <div class=\"card\" style=\"width: 18rem;\">
                <img src=\"{{ vich_uploader_asset(articles) }}\" width=\"18rem\" height=\"20%\"class=\"card-img-top\" alt=\"{{ articles.imageName }}\">
                <div class=\"card-body\">
                    <h5 class=\"card-title\">{{ articles.articleTitle }}</h5>
                    <p class=\"card-text\">{{ articles.contentArticle }}</p>
                    <a href=\"{{  path('article', { 'index':articles.id }) }}\" class=\"btn btn-primary\">Voir l'article</a>
                </div>
            </div>
        {% endfor %}

        </div>
        

    </div>

    

{% endblock %}

<style>
.container_search
{
    margin-top: 2%;
    width: 100%;
    position: flex;
}
.container_under
{
    width: 30%;
    margin: auto;
    
}

.container_article
{
    height: 100%;
}
.container_card
{
    display: flex;
    margin: 5%;
    flex-wrap: wrap;
}
.card
{
    margin-top: 2%;
    margin-left: 5%;
}

.card-text
{
    white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  max-width: 200px;
}
</style>", "Home/index.html.twig", "/Users/dencis/Dev/blogSymfony/templates/Home/index.html.twig");
    }
}
