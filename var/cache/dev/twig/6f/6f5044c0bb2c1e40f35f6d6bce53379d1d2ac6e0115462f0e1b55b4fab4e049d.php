<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* Admin/index.html.twig */
class __TwigTemplate_a6a9ad9e47044ffbe96516ae3c359633c75f08947c471f7ae1844197be926950 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "Admin/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "Admin/index.html.twig"));

        // line 1
        echo "
";
        // line 2
        $this->loadTemplate("_nav.html.twig", "Admin/index.html.twig", 2)->display($context);
        // line 3
        $this->loadTemplate("_success_bar.html.twig", "Admin/index.html.twig", 3)->display($context);
        // line 4
        echo "
";
        // line 5
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 5, $this->source); })()), "flashes", [0 => "success"], "method", false, false, false, 5));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 6
            echo "   <div class=\"alert alert-success\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 6, $this->source); })()), "flashes", [0 => "success"], "method", false, false, false, 6), "html", null, true);
            echo "</div>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 8
        echo "

 ";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 10, $this->source); })()), "flashes", [0 => "updateArticle"], "method", false, false, false, 10));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 11
            echo "            <div class=\"alert alert-success\">
                ";
            // line 12
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "
            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 15
        echo "

 ";
        // line 17
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 17, $this->source); })()), "flashes", [0 => "deleteArticle"], "method", false, false, false, 17));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 18
            echo "            <div class=\"alert alert-success\">
                ";
            // line 19
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "
            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 22
        echo "
<div class=\"container-md\">
    <div class=\"row row-cols-2\">
        <div class=\"col-4\"><h5>Espace Admin</h5></div>
        <div class=\"col-4\"><h5 >Bienvenue ";
        // line 26
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 26, $this->source); })()), "user", [], "any", false, false, false, 26), "username", [], "any", false, false, false, 26), "html", null, true);
        echo ", vous êtes sur l'espace admin</h5></div>
           
        <div class=\"col-4\"><a href=\"";
        // line 28
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_logoutMember");
        echo "\"><h5>Vous déconnectez</h5></a>
    </div>

    </div>

</div>

<div class=\"container-md\">
   <div class=\"row justify-content-center\">
        <ul class=\"nav\">
            <div class=\"col-6\">
                <li class=\"nav-item\">
                    <a class=\"nav-link active\" href=\"";
        // line 40
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("add_news");
        echo "\">Ajouter</a>
                </li>
            </div>
            <div class=\"col-6\">
                <li class=\"nav-item\">
                    <a class=\"nav-link active\" href=\"";
        // line 45
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("profil", ["username" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 45, $this->source); })()), "user", [], "any", false, false, false, 45), "username", [], "any", false, false, false, 45)]), "html", null, true);
        echo "\">Profil</a>
                </li>
            </div>
               
               
            </ul>
       
   </div>
</div>

<div class=\"container-md\">

    <div class=\"jumbotron\">
      <div class=\"row\">

    ";
        // line 60
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["pagination"]) || array_key_exists("pagination", $context) ? $context["pagination"] : (function () { throw new RuntimeError('Variable "pagination" does not exist.', 60, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["article"]) {
            // line 61
            echo "        <div class=\"col-sm-3\">
        
            <div class=\"card\">
                <img src=\"";
            // line 64
            echo twig_escape_filter($this->env, $this->extensions['Vich\UploaderBundle\Twig\Extension\UploaderExtension']->asset($context["article"]), "html", null, true);
            echo "\" height=\"20%\"class=\"card-img-top\" alt=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["article"], "imageName", [], "any", false, false, false, 64), "html", null, true);
            echo "\">
                <div class=\"card-body\">
                    <h5 class=\"card-title\">";
            // line 66
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["article"], "articleTitle", [], "any", false, false, false, 66), "html", null, true);
            echo "</h5>
                    <p class=\"card-text\">";
            // line 67
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["article"], "contentArticle", [], "any", false, false, false, 67), "html", null, true);
            echo "</p>
                    <div class=\"container-md\">
                        <div class=\"row\">
                                <a href=\"";
            // line 70
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("article", ["index" => twig_get_attribute($this->env, $this->source, $context["article"], "id", [], "any", false, false, false, 70)]), "html", null, true);
            echo "\" class=\"btn btn-primary btn-sm\">Voir l'article</a>
                                <a href=\"";
            // line 71
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("updateArticle", ["id" => twig_get_attribute($this->env, $this->source, $context["article"], "id", [], "any", false, false, false, 71)]), "html", null, true);
            echo "\" class=\"btn btn-primary btn-sm\">Modifier </a>
                                <a href=\"";
            // line 72
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("deleteOneArticle", ["id" => twig_get_attribute($this->env, $this->source, $context["article"], "id", [], "any", false, false, false, 72)]), "html", null, true);
            echo "\" class=\"btn btn-primary btn-sm\">Supprimer </a>

               
                        </div>
                      
                    </div>
                   
                  
                </div>
            </div>
        </div>
        
     ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['article'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 85
        echo "

      
       </div>
    </div>
</div>

<div class=\"container-md\">
    <div class=\"row justify-content-center\">
    
        <div class=\"col-3\">
            <nav aria-label=\"Page navigation example\">
                <ul class=\"pagination\">
                    ";
        // line 98
        echo $this->extensions['Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension']->render($this->env, (isset($context["pagination"]) || array_key_exists("pagination", $context) ? $context["pagination"] : (function () { throw new RuntimeError('Variable "pagination" does not exist.', 98, $this->source); })()));
        echo "

                </ul>

            </nav>
        </div>
    </div>
    
</div>


<div class=\"container-md\">
    <div class=\"row justify-content-center\">
        <div class=\"col-3\">
            <h4>Journal de bord</h4>
        
        </div>
    
    </div>
</div>

<div class=\"container-md\">
     <div class=\"jumbotron\">

        <div class=\"row justify-content-center\">
                <div class=\"col\">
                     <h2>Derniers articles crées</h2>
 
                            ";
        // line 126
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["lastArticlesCreated"]) || array_key_exists("lastArticlesCreated", $context) ? $context["lastArticlesCreated"] : (function () { throw new RuntimeError('Variable "lastArticlesCreated" does not exist.', 126, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["article"]) {
            // line 127
            echo "                                <a href=\" ";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("article", ["index" => twig_get_attribute($this->env, $this->source, $context["article"], "id", [], "any", false, false, false, 127)]), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["article"], "ArticleTitle", [], "any", false, false, false, 127), "html", null, true);
            echo " </a> le <b> ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["article"], "CreationDate", [], "any", false, false, false, 127), "m/d/Y"), "html", null, true);
            echo " ,  </b>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['article'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 129
        echo "                      
                <div class=\"col\">
                    <h2>Dernier nouveaux utilisateurs</h2>
                    ";
        // line 132
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["lastUsersRegister"]) || array_key_exists("lastUsersRegister", $context) ? $context["lastUsersRegister"] : (function () { throw new RuntimeError('Variable "lastUsersRegister" does not exist.', 132, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
            // line 133
            echo "                                  [ <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("visitor_profil", ["username" => twig_get_attribute($this->env, $this->source, $context["user"], "username", [], "any", false, false, false, 133)]), "html", null, true);
            echo "\"> ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "username", [], "any", false, false, false, 133), "html", null, true);
            echo " </a> ]
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 135
        echo "                
                </div>


                      
                      


                </div>
        </div>

    </div>
</div>



<style>
.card-text
{
    white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  max-width: 200px;

}

</style>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "Admin/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  294 => 135,  283 => 133,  279 => 132,  274 => 129,  261 => 127,  257 => 126,  226 => 98,  211 => 85,  192 => 72,  188 => 71,  184 => 70,  178 => 67,  174 => 66,  167 => 64,  162 => 61,  158 => 60,  140 => 45,  132 => 40,  117 => 28,  112 => 26,  106 => 22,  97 => 19,  94 => 18,  90 => 17,  86 => 15,  77 => 12,  74 => 11,  70 => 10,  66 => 8,  57 => 6,  53 => 5,  50 => 4,  48 => 3,  46 => 2,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("
{% include \"_nav.html.twig\" %}
{% include \"_success_bar.html.twig\" %}

{% for message in app.flashes('success') %}
   <div class=\"alert alert-success\">{{  app.flashes('success') }}</div>
{% endfor %}


 {% for message in app.flashes('updateArticle') %}
            <div class=\"alert alert-success\">
                {{ message }}
            </div>
        {% endfor %}


 {% for message in app.flashes('deleteArticle') %}
            <div class=\"alert alert-success\">
                {{ message }}
            </div>
        {% endfor %}

<div class=\"container-md\">
    <div class=\"row row-cols-2\">
        <div class=\"col-4\"><h5>Espace Admin</h5></div>
        <div class=\"col-4\"><h5 >Bienvenue {{ app.user.username }}, vous êtes sur l'espace admin</h5></div>
           
        <div class=\"col-4\"><a href=\"{{ path('app_logoutMember') }}\"><h5>Vous déconnectez</h5></a>
    </div>

    </div>

</div>

<div class=\"container-md\">
   <div class=\"row justify-content-center\">
        <ul class=\"nav\">
            <div class=\"col-6\">
                <li class=\"nav-item\">
                    <a class=\"nav-link active\" href=\"{{ path('add_news') }}\">Ajouter</a>
                </li>
            </div>
            <div class=\"col-6\">
                <li class=\"nav-item\">
                    <a class=\"nav-link active\" href=\"{{ path('profil', {'username': app.user.username }) }}\">Profil</a>
                </li>
            </div>
               
               
            </ul>
       
   </div>
</div>

<div class=\"container-md\">

    <div class=\"jumbotron\">
      <div class=\"row\">

    {% for article in pagination %}
        <div class=\"col-sm-3\">
        
            <div class=\"card\">
                <img src=\"{{ vich_uploader_asset(article) }}\" height=\"20%\"class=\"card-img-top\" alt=\"{{ article.imageName }}\">
                <div class=\"card-body\">
                    <h5 class=\"card-title\">{{ article.articleTitle }}</h5>
                    <p class=\"card-text\">{{ article.contentArticle }}</p>
                    <div class=\"container-md\">
                        <div class=\"row\">
                                <a href=\"{{ path('article', {'index':article.id })}}\" class=\"btn btn-primary btn-sm\">Voir l'article</a>
                                <a href=\"{{ path('updateArticle', {'id':article.id })}}\" class=\"btn btn-primary btn-sm\">Modifier </a>
                                <a href=\"{{ path('deleteOneArticle', {'id':article.id })}}\" class=\"btn btn-primary btn-sm\">Supprimer </a>

               
                        </div>
                      
                    </div>
                   
                  
                </div>
            </div>
        </div>
        
     {% endfor %}


      
       </div>
    </div>
</div>

<div class=\"container-md\">
    <div class=\"row justify-content-center\">
    
        <div class=\"col-3\">
            <nav aria-label=\"Page navigation example\">
                <ul class=\"pagination\">
                    {{ knp_pagination_render(pagination) }}

                </ul>

            </nav>
        </div>
    </div>
    
</div>


<div class=\"container-md\">
    <div class=\"row justify-content-center\">
        <div class=\"col-3\">
            <h4>Journal de bord</h4>
        
        </div>
    
    </div>
</div>

<div class=\"container-md\">
     <div class=\"jumbotron\">

        <div class=\"row justify-content-center\">
                <div class=\"col\">
                     <h2>Derniers articles crées</h2>
 
                            {% for article in lastArticlesCreated %}
                                <a href=\" {{ path('article', {'index':article.id }) }}\">{{ article.ArticleTitle }} </a> le <b> {{ article.CreationDate|date(\"m/d/Y\") }} ,  </b>
                            {% endfor %}
                      
                <div class=\"col\">
                    <h2>Dernier nouveaux utilisateurs</h2>
                    {% for user in lastUsersRegister %}
                                  [ <a href=\"{{ path( 'visitor_profil', {'username':user.username })}}\"> {{ user.username }} </a> ]
                    {% endfor %}
                
                </div>


                      
                      


                </div>
        </div>

    </div>
</div>



<style>
.card-text
{
    white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  max-width: 200px;

}

</style>", "Admin/index.html.twig", "/Users/dencis/Dev/blogSymfony/templates/Admin/index.html.twig");
    }
}
