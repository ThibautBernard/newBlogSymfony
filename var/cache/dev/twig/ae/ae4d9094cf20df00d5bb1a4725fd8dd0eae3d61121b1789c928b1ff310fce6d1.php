<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* SpaceMember/Profil/homeVisiteur.html.twig */
class __TwigTemplate_89b2c5555f74ae727f0848607fbceaaf8cde9070b1d39dd970fe3f39d8384f15 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "SpaceMember/Profil/homeVisiteur.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "SpaceMember/Profil/homeVisiteur.html.twig"));

        // line 1
        $this->loadTemplate("__bootstrap.html.twig", "SpaceMember/Profil/homeVisiteur.html.twig", 1)->display($context);
        // line 2
        echo "

";
        // line 4
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 4, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["users"]) {
            // line 5
            echo "
<div class=\"container-md\">
    <h2>
        Page profil de ";
            // line 8
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["users"], "userName", [], "any", false, false, false, 8), "html", null, true);
            echo "
    </h2>

";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['users'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 12
        echo "



";
        // line 16
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 16, $this->source); })()), "flashes", [0 => "modifRoles"], "method", false, false, false, 16));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 17
            echo "     <div class=\"alert alert-success\"> ";
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "</div>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 19
        echo "
<div class=\"container-md\">
    <h5>
        <a href=\"";
        // line 22
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("member_space");
        echo "\">Retour</a>
    </h5>

</div>

";
        // line 27
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 27, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["users"]) {
            // line 28
            echo "


    <div class=\"container-md\">
        <div class=\"jumbotron\">
        

            <div class=\"container-md\">
            ";
            // line 36
            if ((isset($context["profile"]) || array_key_exists("profile", $context))) {
                // line 37
                echo "
                <img src=\"";
                // line 38
                echo twig_escape_filter($this->env, $this->extensions['Vich\UploaderBundle\Twig\Extension\UploaderExtension']->asset((isset($context["profile"]) || array_key_exists("profile", $context) ? $context["profile"] : (function () { throw new RuntimeError('Variable "profile" does not exist.', 38, $this->source); })()), "imageFile"), "html", null, true);
                echo "\" height=\"20%\" class=\"card-img-top\"  alt=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["profile"]) || array_key_exists("profile", $context) ? $context["profile"] : (function () { throw new RuntimeError('Variable "profile" does not exist.', 38, $this->source); })()), "imageName", [], "any", false, false, false, 38), "html", null, true);
                echo "\"></img>

            ";
            }
            // line 41
            echo "            </div>
            <div class=\"row justify-content-center\">
                
            <div class=\"container-md\"></div> 
                <div class=\"col\">
                    <h2>Ces informations</h2>
                </div>
            </div>


                <div class=\"col\">

                   


                </div>
                <div class=\"col\">
                   <b> Pseudo </b> :  ";
            // line 58
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["users"], "userName", [], "any", false, false, false, 58), "html", null, true);
            echo "
                </div>
                    ";
            // line 60
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ADMIN")) {
                // line 61
                echo "                <div class=\"col\">
                   <b> Email </b> :  ";
                // line 62
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["users"], "email", [], "any", false, false, false, 62), "html", null, true);
                echo "
                </div>
                    ";
            } else {
                // line 65
                echo "                        Tu n'es pas admin pour voir ça
                    ";
            }
            // line 67
            echo "

                <div class=\"col\">
                 
                ";
            // line 71
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["users"], "roles", [], "any", false, false, false, 71));
            foreach ($context['_seq'] as $context["_key"] => $context["role"]) {
                echo "    

                   <b> Role </b> : ";
                // line 73
                echo twig_escape_filter($this->env, $context["role"], "html", null, true);
                echo " 
                    ";
                // line 74
                if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ADMIN")) {
                    // line 75
                    echo "                        <a href=\"";
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("update_roles", ["username" => twig_get_attribute($this->env, $this->source, $context["users"], "username", [], "any", false, false, false, 75)]), "html", null, true);
                    echo "\">Modifier son rôle</a>
                    ";
                }
                // line 77
                echo "                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['role'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 78
            echo "                </div>

            
            </div>
        
        </div>
    </div>



";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['users'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 88
        echo "   

";
        // line 90
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 90, $this->source); })()), "flashes", [0 => "error"], "method", false, false, false, 90));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 91
            echo "            <div class=\"alert alert-success\">
                ";
            // line 92
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "
            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 95
        echo "
<div class=\"container-md\">


";
        // line 99
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 99, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["users"]) {
            // line 100
            echo "    ";
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ADMIN")) {
                // line 101
                echo "        <a href=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("profil_delete", ["username" => twig_get_attribute($this->env, $this->source, $context["users"], "username", [], "any", false, false, false, 101)]), "html", null, true);
                echo "\">Supprimer son profil</a>

    ";
            }
            // line 104
            echo "
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['users'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 106
        echo "
</div>


<style>
.card-img-top
{
    width: 20%;
}
</style>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "SpaceMember/Profil/homeVisiteur.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  260 => 106,  253 => 104,  246 => 101,  243 => 100,  239 => 99,  233 => 95,  224 => 92,  221 => 91,  217 => 90,  213 => 88,  197 => 78,  191 => 77,  185 => 75,  183 => 74,  179 => 73,  172 => 71,  166 => 67,  162 => 65,  156 => 62,  153 => 61,  151 => 60,  146 => 58,  127 => 41,  119 => 38,  116 => 37,  114 => 36,  104 => 28,  100 => 27,  92 => 22,  87 => 19,  78 => 17,  74 => 16,  68 => 12,  58 => 8,  53 => 5,  49 => 4,  45 => 2,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% include '__bootstrap.html.twig' %}


{% for users in user %}

<div class=\"container-md\">
    <h2>
        Page profil de {{ users.userName }}
    </h2>

{% endfor %}




{% for message in app.flashes('modifRoles') %}
     <div class=\"alert alert-success\"> {{ message }}</div>
{% endfor %}

<div class=\"container-md\">
    <h5>
        <a href=\"{{ path('member_space') }}\">Retour</a>
    </h5>

</div>

{% for users in user %}



    <div class=\"container-md\">
        <div class=\"jumbotron\">
        

            <div class=\"container-md\">
            {% if profile is defined %}

                <img src=\"{{ vich_uploader_asset(profile, 'imageFile') }}\" height=\"20%\" class=\"card-img-top\"  alt=\"{{ profile.imageName }}\"></img>

            {% endif %}
            </div>
            <div class=\"row justify-content-center\">
                
            <div class=\"container-md\"></div> 
                <div class=\"col\">
                    <h2>Ces informations</h2>
                </div>
            </div>


                <div class=\"col\">

                   


                </div>
                <div class=\"col\">
                   <b> Pseudo </b> :  {{ users.userName }}
                </div>
                    {% if is_granted('ROLE_ADMIN') %}
                <div class=\"col\">
                   <b> Email </b> :  {{ users.email }}
                </div>
                    {% else %}
                        Tu n'es pas admin pour voir ça
                    {% endif %}


                <div class=\"col\">
                 
                {% for role in users.roles %}    

                   <b> Role </b> : {{ role }} 
                    {% if is_granted('ROLE_ADMIN') %}
                        <a href=\"{{ path('update_roles', {'username': users.username }) }}\">Modifier son rôle</a>
                    {% endif %}
                {% endfor %}
                </div>

            
            </div>
        
        </div>
    </div>



{% endfor %}   

{% for message in app.flashes('error') %}
            <div class=\"alert alert-success\">
                {{ message }}
            </div>
        {% endfor %}

<div class=\"container-md\">


{% for users in user %}
    {% if is_granted('ROLE_ADMIN') %}
        <a href=\"{{ path('profil_delete', {'username': users.username }) }}\">Supprimer son profil</a>

    {% endif %}

{% endfor %}

</div>


<style>
.card-img-top
{
    width: 20%;
}
</style>", "SpaceMember/Profil/homeVisiteur.html.twig", "/Users/dencis/Dev/blogSymfony/templates/SpaceMember/Profil/homeVisiteur.html.twig");
    }
}
