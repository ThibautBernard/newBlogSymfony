<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* Home/article.html.twig */
class __TwigTemplate_3f7414dc794b675ec5e105137b35dd9780026319b81311f088f86bda3a801842 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "Home/article.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "Home/article.html.twig"));

        // line 1
        $this->loadTemplate("_nav.html.twig", "Home/article.html.twig", 1)->display($context);
        echo " 


";
        // line 4
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["article"]) || array_key_exists("article", $context) ? $context["article"] : (function () { throw new RuntimeError('Variable "article" does not exist.', 4, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["articles"]) {
            // line 5
            echo "          
          
      <div class=\"container-page\">
      
            <div class=\"container-img\">
                <img src=\"";
            // line 10
            echo twig_escape_filter($this->env, $this->extensions['Vich\UploaderBundle\Twig\Extension\UploaderExtension']->asset($context["articles"]), "html", null, true);
            echo "\" class=\"card-img-top\" alt=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["articles"], "imageName", [], "any", false, false, false, 10), "html", null, true);
            echo "\">
            </div>
        </div>


        <div class=\"container-title\">
                <div class=\"container-title_align\">
                    <h2 class=\"card-title\">";
            // line 17
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["articles"], "articleTitle", [], "any", false, false, false, 17), "html", null, true);
            echo "</h2>
                </div>
        </div>

        <div class=\"container-author-date\">

            <div class=\"align-item\">
                <div class=\"container-dateCreated\">
                    <p class=\"card-text\">";
            // line 25
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["articles"], "creationDate", [], "any", false, false, false, 25), "d/m/Y", "Europe/Paris"), "html", null, true);
            echo "</p>

                </div>

                <div class=\"container-author\">
                       <p class=\"card-text\">Author</p>

                </div>

                <div class=\"container-updatedDate\">
                    <p class=\"card-text\">";
            // line 35
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["articles"], "updatedDate", [], "any", false, false, false, 35), "d/m/Y", "Europe/Paris"), "html", null, true);
            echo "</p>

                </div>
            </div>


            </div>
            <div class=\"container-content-article\">
                <div class=\"article-content\">
                        <p>";
            // line 44
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["articles"], "contentArticle", [], "any", false, false, false, 44), "html", null, true);
            echo "</p>
                </div>
        </div>
        </div>


    </div>          




<div class=\"container-md\">
    <div class=\"row justify-content-center\">
        <div class=\"col-3\">
            ";
            // line 58
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ADMIN")) {
                // line 59
                echo "                <a href=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("delete_one_news", ["id" => twig_get_attribute($this->env, $this->source, $context["articles"], "id", [], "any", false, false, false, 59)]), "html", null, true);
                echo "\">Supprimer cet article</a>
            ";
            }
            // line 61
            echo "
        </div>

        <div class=\"col-3\">
             <a href=\"";
            // line 65
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("home");
            echo "\">Retour à la page principal </a>

        </div>
    </div>

</div>

";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['articles'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 73
        echo "
<style>
    .container-page
    {
        display: flex;
        flex-direction: column;
    }

    .container-img
    {
        margin: 2%;
    }


    img {
    width: 100%;
    height: 50%;
    }
    .container-title
    {
        display: flex;
        height: 10%;
        margin-left:2%;
        margin-right:2%;

    }
    .container-title_align
    {
        margin: auto;
    }


    .container-author-date
    {
        margin-left: 2%;
        margin-right: 2%;
        display: flex;
    }
    .align-item
    {
        display:flex;
        margin: auto;
        
    }

    .container-author
    {
        margin-left: 2%;
        


    }

    .container-dateCreated p
    {
        font-style: italic;

    }

    .container-author p
    {
        font-weight: bold;

    }
    .container-updatedDate
    {
        margin-left: 1%;


    }

    .container-updatedDate p
    {
        font-style: italic;
        display: flex;

    }


    .container-content-article
    {
        margin-left: 2%;
        margin-right: 2%;
        margin-top: 5%;
    }

    .article-content
    {
        width: 50%;
        margin: auto;
    }
    .article-content p
    {
        text-align: center;
    }

   

</style>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "Home/article.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  153 => 73,  139 => 65,  133 => 61,  127 => 59,  125 => 58,  108 => 44,  96 => 35,  83 => 25,  72 => 17,  60 => 10,  53 => 5,  49 => 4,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% include(\"_nav.html.twig\") %} 


{% for articles in article %}
          
          
      <div class=\"container-page\">
      
            <div class=\"container-img\">
                <img src=\"{{ vich_uploader_asset(articles) }}\" class=\"card-img-top\" alt=\"{{ articles.imageName }}\">
            </div>
        </div>


        <div class=\"container-title\">
                <div class=\"container-title_align\">
                    <h2 class=\"card-title\">{{ articles.articleTitle }}</h2>
                </div>
        </div>

        <div class=\"container-author-date\">

            <div class=\"align-item\">
                <div class=\"container-dateCreated\">
                    <p class=\"card-text\">{{ articles.creationDate|date(\"d/m/Y\", \"Europe/Paris\") }}</p>

                </div>

                <div class=\"container-author\">
                       <p class=\"card-text\">Author</p>

                </div>

                <div class=\"container-updatedDate\">
                    <p class=\"card-text\">{{  articles.updatedDate|date(\"d/m/Y\", \"Europe/Paris\")  }}</p>

                </div>
            </div>


            </div>
            <div class=\"container-content-article\">
                <div class=\"article-content\">
                        <p>{{ articles.contentArticle }}</p>
                </div>
        </div>
        </div>


    </div>          




<div class=\"container-md\">
    <div class=\"row justify-content-center\">
        <div class=\"col-3\">
            {% if is_granted('ROLE_ADMIN') %}
                <a href=\"{{ path('delete_one_news', {'id': articles.id }) }}\">Supprimer cet article</a>
            {% endif %}

        </div>

        <div class=\"col-3\">
             <a href=\"{{ path('home') }}\">Retour à la page principal </a>

        </div>
    </div>

</div>

{% endfor %}

<style>
    .container-page
    {
        display: flex;
        flex-direction: column;
    }

    .container-img
    {
        margin: 2%;
    }


    img {
    width: 100%;
    height: 50%;
    }
    .container-title
    {
        display: flex;
        height: 10%;
        margin-left:2%;
        margin-right:2%;

    }
    .container-title_align
    {
        margin: auto;
    }


    .container-author-date
    {
        margin-left: 2%;
        margin-right: 2%;
        display: flex;
    }
    .align-item
    {
        display:flex;
        margin: auto;
        
    }

    .container-author
    {
        margin-left: 2%;
        


    }

    .container-dateCreated p
    {
        font-style: italic;

    }

    .container-author p
    {
        font-weight: bold;

    }
    .container-updatedDate
    {
        margin-left: 1%;


    }

    .container-updatedDate p
    {
        font-style: italic;
        display: flex;

    }


    .container-content-article
    {
        margin-left: 2%;
        margin-right: 2%;
        margin-top: 5%;
    }

    .article-content
    {
        width: 50%;
        margin: auto;
    }
    .article-content p
    {
        text-align: center;
    }

   

</style>", "Home/article.html.twig", "/Users/dencis/Dev/blogSymfony/templates/Home/article.html.twig");
    }
}
