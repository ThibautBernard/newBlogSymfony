<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* SpaceMember/Home/index.html.twig */
class __TwigTemplate_7907bfb88cda9dc84f1b53364dc1789fb6e21667800e7f503c2a3d37a0447849 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "SpaceMember/Home/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "SpaceMember/Home/index.html.twig"));

        // line 1
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ADMIN")) {
            // line 2
            echo "    ";
            $this->loadTemplate("Admin/index.html.twig", "SpaceMember/Home/index.html.twig", 2)->display($context);
        }
        // line 4
        echo "

";
        // line 6
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_USER")) {
            // line 7
            echo "    ";
            if ((0 !== twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 7, $this->source); })()), "user", [], "any", false, false, false, 7), "token", [], "any", false, false, false, 7), "null"))) {
                // line 8
                echo "        ";
                $this->loadTemplate("SpaceMember/signin/verif.html.twig", "SpaceMember/Home/index.html.twig", 8)->display($context);
                // line 9
                echo "     ";
            }
            // line 10
            echo "
    ";
            // line 11
            if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 11, $this->source); })()), "user", [], "any", false, false, false, 11), "token", [], "any", false, false, false, 11), "null"))) {
                // line 12
                echo "         ";
                $this->loadTemplate("SpaceMember/Home/home.html.twig", "SpaceMember/Home/index.html.twig", 12)->display($context);
                // line 13
                echo "     ";
            }
            // line 14
            echo "   
";
        }
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "SpaceMember/Home/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  75 => 14,  72 => 13,  69 => 12,  67 => 11,  64 => 10,  61 => 9,  58 => 8,  55 => 7,  53 => 6,  49 => 4,  45 => 2,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% if is_granted('ROLE_ADMIN') %}
    {% include 'Admin/index.html.twig' %}
{% endif %}


{% if is_granted('ROLE_USER') %}
    {% if app.user.token != 'null' %}
        {% include 'SpaceMember/signin/verif.html.twig' %}
     {% endif %}

    {% if app.user.token == 'null' %}
         {% include 'SpaceMember/Home/home.html.twig' %}
     {% endif %}
   
{% endif %}
", "SpaceMember/Home/index.html.twig", "/Users/dencis/Dev/blogSymfony/templates/SpaceMember/Home/index.html.twig");
    }
}
