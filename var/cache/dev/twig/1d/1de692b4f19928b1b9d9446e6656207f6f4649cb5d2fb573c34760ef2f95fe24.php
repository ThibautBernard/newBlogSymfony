<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* SpaceMember/Home/home.html.twig */
class __TwigTemplate_a5aea23a5e4b725fa664f6dacb1372cd8c8076670b0a2fa2501f67a6fb12e55f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "SpaceMember/Home/home.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "SpaceMember/Home/home.html.twig"));

        // line 1
        $this->loadTemplate("_nav.html.twig", "SpaceMember/Home/home.html.twig", 1)->display($context);
        // line 2
        $this->loadTemplate("_success_bar.html.twig", "SpaceMember/Home/home.html.twig", 2)->display($context);
        // line 3
        echo "
";
        // line 4
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 4, $this->source); })()), "flashes", [0 => "success"], "method", false, false, false, 4));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 5
            echo "   <div class=\"alert alert-success\">";
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "</div>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 7
        echo "
<div class=\"container-md\">
    <div class=\"row row-cols-2\">
        <div class=\"col-4\"><h5>Espace membre</h5></div>
        <div class=\"col-4\"><h5 >Bienvenue ";
        // line 11
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 11, $this->source); })()), "user", [], "any", false, false, false, 11), "username", [], "any", false, false, false, 11), "html", null, true);
        echo ".</h5></div>
           
        <div class=\"col-4\"><a href=\"";
        // line 13
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_logoutMember");
        echo "\"><h5>Vous déconnectez</h5></a>
    </div>

    </div>

</div>

<div class=\"container-md\">
   <div class=\"row\">
        <ul class=\"nav\">
            <div class=\"col-3\">
                <li class=\"nav-item\">
                    <a class=\"nav-link active\" href=\"#\">Ajouter</a>
                </li>
            </div>
            <div class=\"col-3\">
                <li class=\"nav-item\">
                    <a class=\"nav-link active\" href=\"#\">Modifier</a>
                </li>
            </div>
            <div class=\"col-3\">
                <li class=\"nav-item\">
                    <a class=\"nav-link active\" href=\"#\">Supprimer</a>
                </li>
            </div>
            <div class=\"col-3\">
                <li class=\"nav-item\">
                    <a class=\"nav-link active\" href=\"#\">Profil</a>
                </li>
            </div>
               
               
            </ul>
       
   </div>
</div>

<div class=\"container-md\">

    <div class=\"jumbotron\">
      <div class=\"row\">

        <div class=\"col-sm-3\">
            <div class=\"card\">
                <img src=\"...\" height=\"20%\"class=\"card-img-top\" alt=\"...\">
                <div class=\"card-body\">
                    <h5 class=\"card-title\">Card title</h5>
                    <p class=\"card-text\">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    <a href=\"#\" class=\"btn btn-primary\">Go somewhere</a>
                </div>
            </div>
        </div>

        <div class=\"col-sm-3\">
            <div class=\"card\">
                <img src=\"...\" height=\"20%\"class=\"card-img-top\" alt=\"...\">
                <div class=\"card-body\">
                    <h5 class=\"card-title\">Card title</h5>
                    <p class=\"card-text\">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    <a href=\"#\" class=\"btn btn-primary\">Go somewhere</a>
                </div>
            </div>
        </div>
        <div class=\"col-sm-3\">
            <div class=\"card\">
                <img src=\"...\" height=\"20%\"class=\"card-img-top\" alt=\"...\">
                <div class=\"card-body\">
                    <h5 class=\"card-title\">Card title</h5>
                    <p class=\"card-text\">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    <a href=\"#\" class=\"btn btn-primary\">Go somewhere</a>
                </div>
            </div>
        </div>
        <div class=\"col-sm-3\">
            <div class=\"card\">
                <img src=\"...\" height=\"20%\"class=\"card-img-top\" alt=\"...\">
                <div class=\"card-body\">
                    <h5 class=\"card-title\">Card title</h5>
                    <p class=\"card-text\">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    <a href=\"#\" class=\"btn btn-primary\">Go somewhere</a>
                </div>
            </div>
        </div>
       </div>
    </div>
</div>

";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "SpaceMember/Home/home.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  74 => 13,  69 => 11,  63 => 7,  54 => 5,  50 => 4,  47 => 3,  45 => 2,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% include \"_nav.html.twig\" %}
{% include \"_success_bar.html.twig\" %}

{% for message in app.flashes('success') %}
   <div class=\"alert alert-success\">{{ message }}</div>
{% endfor %}

<div class=\"container-md\">
    <div class=\"row row-cols-2\">
        <div class=\"col-4\"><h5>Espace membre</h5></div>
        <div class=\"col-4\"><h5 >Bienvenue {{ app.user.username }}.</h5></div>
           
        <div class=\"col-4\"><a href=\"{{ path('app_logoutMember') }}\"><h5>Vous déconnectez</h5></a>
    </div>

    </div>

</div>

<div class=\"container-md\">
   <div class=\"row\">
        <ul class=\"nav\">
            <div class=\"col-3\">
                <li class=\"nav-item\">
                    <a class=\"nav-link active\" href=\"#\">Ajouter</a>
                </li>
            </div>
            <div class=\"col-3\">
                <li class=\"nav-item\">
                    <a class=\"nav-link active\" href=\"#\">Modifier</a>
                </li>
            </div>
            <div class=\"col-3\">
                <li class=\"nav-item\">
                    <a class=\"nav-link active\" href=\"#\">Supprimer</a>
                </li>
            </div>
            <div class=\"col-3\">
                <li class=\"nav-item\">
                    <a class=\"nav-link active\" href=\"#\">Profil</a>
                </li>
            </div>
               
               
            </ul>
       
   </div>
</div>

<div class=\"container-md\">

    <div class=\"jumbotron\">
      <div class=\"row\">

        <div class=\"col-sm-3\">
            <div class=\"card\">
                <img src=\"...\" height=\"20%\"class=\"card-img-top\" alt=\"...\">
                <div class=\"card-body\">
                    <h5 class=\"card-title\">Card title</h5>
                    <p class=\"card-text\">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    <a href=\"#\" class=\"btn btn-primary\">Go somewhere</a>
                </div>
            </div>
        </div>

        <div class=\"col-sm-3\">
            <div class=\"card\">
                <img src=\"...\" height=\"20%\"class=\"card-img-top\" alt=\"...\">
                <div class=\"card-body\">
                    <h5 class=\"card-title\">Card title</h5>
                    <p class=\"card-text\">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    <a href=\"#\" class=\"btn btn-primary\">Go somewhere</a>
                </div>
            </div>
        </div>
        <div class=\"col-sm-3\">
            <div class=\"card\">
                <img src=\"...\" height=\"20%\"class=\"card-img-top\" alt=\"...\">
                <div class=\"card-body\">
                    <h5 class=\"card-title\">Card title</h5>
                    <p class=\"card-text\">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    <a href=\"#\" class=\"btn btn-primary\">Go somewhere</a>
                </div>
            </div>
        </div>
        <div class=\"col-sm-3\">
            <div class=\"card\">
                <img src=\"...\" height=\"20%\"class=\"card-img-top\" alt=\"...\">
                <div class=\"card-body\">
                    <h5 class=\"card-title\">Card title</h5>
                    <p class=\"card-text\">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    <a href=\"#\" class=\"btn btn-primary\">Go somewhere</a>
                </div>
            </div>
        </div>
       </div>
    </div>
</div>

", "SpaceMember/Home/home.html.twig", "/Users/dencis/Dev/blogSymfony/templates/SpaceMember/Home/home.html.twig");
    }
}
