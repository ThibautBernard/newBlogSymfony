<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* SpaceMember/Profil/home.html.twig */
class __TwigTemplate_22fbe8c1c4ed02c8d01e536adeb1db0252f4a517f403114870cb6d104ee96d44 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "SpaceMember/Profil/home.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "SpaceMember/Profil/home.html.twig"));

        // line 1
        $this->loadTemplate("__bootstrap.html.twig", "SpaceMember/Profil/home.html.twig", 1)->display($context);
        // line 2
        echo "
<div class=\"container-md\">
    <h2>
        Bienvenue sur votre page profil
    </h2>


</div>

 ";
        // line 11
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 11, $this->source); })()), "flashes", [0 => "add"], "method", false, false, false, 11));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 12
            echo "     <div class=\"alert alert-success\"> ";
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "</div>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 14
        echo "
 ";
        // line 15
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 15, $this->source); })()), "flashes", [0 => "modif"], "method", false, false, false, 15));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 16
            echo "     <div class=\"alert alert-success\"> ";
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "</div>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 18
        echo "
";
        // line 19
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 19, $this->source); })()), "flashes", [0 => "deleteAvatar"], "method", false, false, false, 19));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 20
            echo "     <div class=\"alert alert-success\"> ";
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "</div>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 22
        echo "

<div class=\"container-md\">
    <h5>
        <a href=\"";
        // line 26
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("member_space");
        echo "\">Retour</a>
    </h5>

</div>

";
        // line 31
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 31, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["users"]) {
            // line 32
            echo "


    <div class=\"container-md\">
        <div class=\"jumbotron\">
        

            <div class=\"container-md\">
            ";
            // line 40
            if ((isset($context["profile"]) || array_key_exists("profile", $context))) {
                // line 41
                echo "
                <img src=\"";
                // line 42
                echo twig_escape_filter($this->env, $this->extensions['Vich\UploaderBundle\Twig\Extension\UploaderExtension']->asset((isset($context["profile"]) || array_key_exists("profile", $context) ? $context["profile"] : (function () { throw new RuntimeError('Variable "profile" does not exist.', 42, $this->source); })()), "imageFile"), "html", null, true);
                echo "\" height=\"20%\" class=\"card-img-top\"  alt=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["profile"]) || array_key_exists("profile", $context) ? $context["profile"] : (function () { throw new RuntimeError('Variable "profile" does not exist.', 42, $this->source); })()), "imageName", [], "any", false, false, false, 42), "html", null, true);
                echo "\"></img>
                <a href=\"";
                // line 43
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("delete_avatar", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 43, $this->source); })()), "user", [], "any", false, false, false, 43), "id", [], "any", false, false, false, 43)]), "html", null, true);
                echo "\">Supprimer votre avatar </a>

            ";
            }
            // line 46
            echo "            </div>
            <div class=\"row justify-content-center\">
                
            <div class=\"container-md\"></div> 
                <div class=\"col\">
                    <h2>Vos informations</h2>
                </div>
            </div>


                <div class=\"col\">

                    ";
            // line 58
            if ( !(isset($context["profile"]) || array_key_exists("profile", $context))) {
                // line 59
                echo "                       
                             <a href=\"";
                // line 60
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("avatar", ["username" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 60, $this->source); })()), "user", [], "any", false, false, false, 60), "username", [], "any", false, false, false, 60)]), "html", null, true);
                echo "\">Ajouter un avatar </a>
                    ";
            }
            // line 62
            echo "

                </div>
                <div class=\"col\">
                   <b> Pseudo </b> :  ";
            // line 66
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["users"], "userName", [], "any", false, false, false, 66), "html", null, true);
            echo "
                </div>
                <div class=\"col\">
                   <b> Email </b> :  ";
            // line 69
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["users"], "email", [], "any", false, false, false, 69), "html", null, true);
            echo "
                </div>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['users'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 71
        echo "   

                <div class=\"col\">
                ";
        // line 74
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 74, $this->source); })()), "user", [], "any", false, false, false, 74), "roles", [], "any", false, false, false, 74));
        foreach ($context['_seq'] as $context["_key"] => $context["role"]) {
            echo "    
                   <b> Role </b> : ";
            // line 75
            echo twig_escape_filter($this->env, $context["role"], "html", null, true);
            echo " 
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['role'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 77
        echo "                </div>

                <div class=\"col\">
                    <a href=\"";
        // line 80
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("update", ["username" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 80, $this->source); })()), "user", [], "any", false, false, false, 80), "username", [], "any", false, false, false, 80)]), "html", null, true);
        echo "\">Modifier mes informations</a>
                </div>
                
            </div>
        
        </div>
    </div>





";
        // line 92
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 92, $this->source); })()), "flashes", [0 => "error"], "method", false, false, false, 92));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 93
            echo "            <div class=\"alert alert-success\">
                ";
            // line 94
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "
            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 97
        echo "
<div class=\"container-md\">
    <h5>
        <a href=\"";
        // line 100
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("profil_delete", ["username" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 100, $this->source); })()), "user", [], "any", false, false, false, 100), "username", [], "any", false, false, false, 100)]), "html", null, true);
        echo "\">Supprimer votre profil</a>
    </h5>


</div>


<style>
.card-img-top
{
    width: 20%;
}
</style>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "SpaceMember/Profil/home.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  251 => 100,  246 => 97,  237 => 94,  234 => 93,  230 => 92,  215 => 80,  210 => 77,  202 => 75,  196 => 74,  191 => 71,  182 => 69,  176 => 66,  170 => 62,  165 => 60,  162 => 59,  160 => 58,  146 => 46,  140 => 43,  134 => 42,  131 => 41,  129 => 40,  119 => 32,  115 => 31,  107 => 26,  101 => 22,  92 => 20,  88 => 19,  85 => 18,  76 => 16,  72 => 15,  69 => 14,  60 => 12,  56 => 11,  45 => 2,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% include '__bootstrap.html.twig' %}

<div class=\"container-md\">
    <h2>
        Bienvenue sur votre page profil
    </h2>


</div>

 {% for message in app.flashes('add') %}
     <div class=\"alert alert-success\"> {{ message }}</div>
{% endfor %}

 {% for message in app.flashes('modif') %}
     <div class=\"alert alert-success\"> {{ message }}</div>
{% endfor %}

{% for message in app.flashes('deleteAvatar') %}
     <div class=\"alert alert-success\"> {{ message }}</div>
{% endfor %}


<div class=\"container-md\">
    <h5>
        <a href=\"{{ path('member_space') }}\">Retour</a>
    </h5>

</div>

{% for users in user %}



    <div class=\"container-md\">
        <div class=\"jumbotron\">
        

            <div class=\"container-md\">
            {% if profile is defined %}

                <img src=\"{{ vich_uploader_asset(profile, 'imageFile') }}\" height=\"20%\" class=\"card-img-top\"  alt=\"{{ profile.imageName }}\"></img>
                <a href=\"{{ path('delete_avatar', {'id': app.user.id } )}}\">Supprimer votre avatar </a>

            {% endif %}
            </div>
            <div class=\"row justify-content-center\">
                
            <div class=\"container-md\"></div> 
                <div class=\"col\">
                    <h2>Vos informations</h2>
                </div>
            </div>


                <div class=\"col\">

                    {% if profile is not defined  %}
                       
                             <a href=\"{{ path('avatar', {'username': app.user.username } )}}\">Ajouter un avatar </a>
                    {% endif %}


                </div>
                <div class=\"col\">
                   <b> Pseudo </b> :  {{ users.userName }}
                </div>
                <div class=\"col\">
                   <b> Email </b> :  {{ users.email }}
                </div>
{% endfor %}   

                <div class=\"col\">
                {% for role in app.user.roles %}    
                   <b> Role </b> : {{ role }} 
                {% endfor %}
                </div>

                <div class=\"col\">
                    <a href=\"{{ path('update', {'username': app.user.username } )}}\">Modifier mes informations</a>
                </div>
                
            </div>
        
        </div>
    </div>





{% for message in app.flashes('error') %}
            <div class=\"alert alert-success\">
                {{ message }}
            </div>
        {% endfor %}

<div class=\"container-md\">
    <h5>
        <a href=\"{{ path('profil_delete', {'username': app.user.username }) }}\">Supprimer votre profil</a>
    </h5>


</div>


<style>
.card-img-top
{
    width: 20%;
}
</style>", "SpaceMember/Profil/home.html.twig", "/Users/dencis/Dev/blogSymfony/templates/SpaceMember/Profil/home.html.twig");
    }
}
