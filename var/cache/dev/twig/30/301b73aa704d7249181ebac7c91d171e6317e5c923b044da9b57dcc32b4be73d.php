<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* SpaceMember/Home/home.html.twig */
class __TwigTemplate_5972e2771ca797bc68bc0b73a10525631a7846ca9398e4cf66e828388178a969 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "SpaceMember/Home/home.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "SpaceMember/Home/home.html.twig"));

        // line 1
        $this->loadTemplate("_nav.html.twig", "SpaceMember/Home/home.html.twig", 1)->display($context);
        // line 2
        $this->loadTemplate("_success_bar.html.twig", "SpaceMember/Home/home.html.twig", 2)->display($context);
        // line 3
        echo "
";
        // line 4
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 4, $this->source); })()), "flashes", [0 => "success"], "method", false, false, false, 4));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 5
            echo "   <div class=\"alert alert-success\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 5, $this->source); })()), "flashes", [0 => "success"], "method", false, false, false, 5), "html", null, true);
            echo "</div>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 7
        echo "

<div class=\"container-md\">
    <div class=\"row row-cols-2\">
        <div class=\"col-4\"><h5>Espace membre</h5></div>
        <div class=\"col-4\"><h5 >Bienvenue ";
        // line 12
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 12, $this->source); })()), "user", [], "any", false, false, false, 12), "username", [], "any", false, false, false, 12), "html", null, true);
        echo ".</h5></div>
           
        <div class=\"col-4\"><a href=\"";
        // line 14
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_logoutMember");
        echo "\"><h5>Vous déconnectez</h5></a>
    </div>

    </div>

</div>

<div class=\"container-md\">
   <div class=\"row justify-content-center\">
        <ul class=\"nav\">
            <div class=\"col-6\">
                <li class=\"nav-item\">
                    <a class=\"nav-link active\" href=\"";
        // line 26
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("add_news");
        echo "\">Ajouter</a>
                </li>
            </div>
            <div class=\"col-6\">
                <li class=\"nav-item\">
                    <a class=\"nav-link active\" href=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("profil", ["username" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 31, $this->source); })()), "user", [], "any", false, false, false, 31), "username", [], "any", false, false, false, 31)]), "html", null, true);
        echo "\">Profil</a>
                </li>
            </div>
               
               
            </ul>
       
   </div>
</div>

<div class=\"container-md\">

    <div class=\"jumbotron\">
      <div class=\"row\">

    ";
        // line 46
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["pagination"]) || array_key_exists("pagination", $context) ? $context["pagination"] : (function () { throw new RuntimeError('Variable "pagination" does not exist.', 46, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["article"]) {
            // line 47
            echo "        <div class=\"col-sm-3\">
        
            <div class=\"card\">
                <img src=\"";
            // line 50
            echo twig_escape_filter($this->env, $this->extensions['Vich\UploaderBundle\Twig\Extension\UploaderExtension']->asset($context["article"]), "html", null, true);
            echo "\" height=\"20%\"class=\"card-img-top\" alt=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["article"], "imageName", [], "any", false, false, false, 50), "html", null, true);
            echo "\">
                <div class=\"card-body\">
                    <h5 class=\"card-title\">";
            // line 52
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["article"], "articleTitle", [], "any", false, false, false, 52), "html", null, true);
            echo "</h5>
                    <p class=\"card-text\">";
            // line 53
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["article"], "contentArticle", [], "any", false, false, false, 53), "html", null, true);
            echo "</p>
                    <a href=\"";
            // line 54
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("article", ["index" => twig_get_attribute($this->env, $this->source, $context["article"], "id", [], "any", false, false, false, 54)]), "html", null, true);
            echo "\" class=\"btn btn-primary\">Voir l'article</a>
                </div>
            </div>
        </div>
     ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['article'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 59
        echo "

      
       </div>
    </div>
</div>

<div class=\"container-md\">
    <div class=\"row justify-content-center\">
    
        <div class=\"col-3\">
            <nav aria-label=\"Page navigation example\">
                <ul class=\"pagination\">
                    ";
        // line 72
        echo $this->extensions['Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension']->render($this->env, (isset($context["pagination"]) || array_key_exists("pagination", $context) ? $context["pagination"] : (function () { throw new RuntimeError('Variable "pagination" does not exist.', 72, $this->source); })()));
        echo "

                </ul>

            </nav>
        </div>
    </div>
    
</div>





<style>
.card-text
{
    white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  max-width: 200px;

}

</style>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "SpaceMember/Home/home.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  166 => 72,  151 => 59,  140 => 54,  136 => 53,  132 => 52,  125 => 50,  120 => 47,  116 => 46,  98 => 31,  90 => 26,  75 => 14,  70 => 12,  63 => 7,  54 => 5,  50 => 4,  47 => 3,  45 => 2,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% include \"_nav.html.twig\" %}
{% include \"_success_bar.html.twig\" %}

{% for message in app.flashes('success') %}
   <div class=\"alert alert-success\">{{  app.flashes('success') }}</div>
{% endfor %}


<div class=\"container-md\">
    <div class=\"row row-cols-2\">
        <div class=\"col-4\"><h5>Espace membre</h5></div>
        <div class=\"col-4\"><h5 >Bienvenue {{ app.user.username }}.</h5></div>
           
        <div class=\"col-4\"><a href=\"{{ path('app_logoutMember') }}\"><h5>Vous déconnectez</h5></a>
    </div>

    </div>

</div>

<div class=\"container-md\">
   <div class=\"row justify-content-center\">
        <ul class=\"nav\">
            <div class=\"col-6\">
                <li class=\"nav-item\">
                    <a class=\"nav-link active\" href=\"{{ path('add_news') }}\">Ajouter</a>
                </li>
            </div>
            <div class=\"col-6\">
                <li class=\"nav-item\">
                    <a class=\"nav-link active\" href=\"{{ path('profil', {'username': app.user.username }) }}\">Profil</a>
                </li>
            </div>
               
               
            </ul>
       
   </div>
</div>

<div class=\"container-md\">

    <div class=\"jumbotron\">
      <div class=\"row\">

    {% for article in pagination %}
        <div class=\"col-sm-3\">
        
            <div class=\"card\">
                <img src=\"{{ vich_uploader_asset(article) }}\" height=\"20%\"class=\"card-img-top\" alt=\"{{ article.imageName }}\">
                <div class=\"card-body\">
                    <h5 class=\"card-title\">{{ article.articleTitle }}</h5>
                    <p class=\"card-text\">{{ article.contentArticle }}</p>
                    <a href=\"{{ path('article', {'index':article.id })}}\" class=\"btn btn-primary\">Voir l'article</a>
                </div>
            </div>
        </div>
     {% endfor %}


      
       </div>
    </div>
</div>

<div class=\"container-md\">
    <div class=\"row justify-content-center\">
    
        <div class=\"col-3\">
            <nav aria-label=\"Page navigation example\">
                <ul class=\"pagination\">
                    {{ knp_pagination_render(pagination) }}

                </ul>

            </nav>
        </div>
    </div>
    
</div>





<style>
.card-text
{
    white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  max-width: 200px;

}

</style>", "SpaceMember/Home/home.html.twig", "/Users/dencis/Dev/blogSymfony/templates/SpaceMember/Home/home.html.twig");
    }
}
